﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MoneyTrack
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl AddSubtract { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Go { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel hs1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel hs2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel hs3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel hs4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MoneyAll { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField MoneyEntered { get; set; }

        [Action ("Go_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Go_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddSubtract != null) {
                AddSubtract.Dispose ();
                AddSubtract = null;
            }

            if (Go != null) {
                Go.Dispose ();
                Go = null;
            }

            if (hs1 != null) {
                hs1.Dispose ();
                hs1 = null;
            }

            if (hs2 != null) {
                hs2.Dispose ();
                hs2 = null;
            }

            if (hs3 != null) {
                hs3.Dispose ();
                hs3 = null;
            }

            if (hs4 != null) {
                hs4.Dispose ();
                hs4 = null;
            }

            if (MoneyAll != null) {
                MoneyAll.Dispose ();
                MoneyAll = null;
            }

            if (MoneyEntered != null) {
                MoneyEntered.Dispose ();
                MoneyEntered = null;
            }
        }
    }
}