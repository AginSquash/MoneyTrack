﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Foundation;
using UIKit;

namespace MoneyTrack
{
    class SaveLoad
    {
        
        public void Save(string MoneyAll)
        {
            Console.WriteLine("MoneyAll in SaveLoad: " + MoneyAll);
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            XmlTextWriter invWriter = new XmlTextWriter(documentsPath + "/profile.xml", System.Text.Encoding.UTF8);
            invWriter.WriteStartDocument();
            invWriter.WriteStartElement("head");
            invWriter.WriteEndElement();
            invWriter.Close();
            XmlDocument document = new XmlDocument();
            document.Load(documentsPath + "/profile.xml");
            XmlNode element = document.CreateElement("Settings");
            document.DocumentElement.AppendChild(element); // указываем родителя
            XmlAttribute attribute = document.CreateAttribute("name"); // создаём атрибут
            attribute.Value = "Atribute"; // устанавливаем значение атрибута

            XmlNode x = document.CreateElement("MoneyAll"); // даём имя
            x.InnerText = MoneyAll; // и значение
            element.AppendChild(x); // и указываем кому принадлежит

            document.Save(documentsPath + "/profile.xml");

            Console.WriteLine("Succefull write!");
        }

        public string Load()
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            string[] loaded_data = new string[10];
            XmlDocument doc = new XmlDocument();
            doc.Load(documentsPath + "/profile.xml");
            XmlNode child = doc.SelectSingleNode("head/Settings");
            if (child != null)
            {
                Console.WriteLine("Child != null");
                XmlNodeReader nr = new XmlNodeReader(child);
                int i = 0;
                int num = 0;
                while (nr.Read())
                    if (i == 2)
                    {
                        loaded_data[num] = nr.Value;
                        Console.WriteLine("Loaded data: " + loaded_data[num]);
                        return loaded_data[num];
                        num++;
                        i = 0;
                    }
                    else
                    {
                        i++;
                    }
            }

            return "Error";
        }
    }
}