﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using System.Xml;
using System.IO;


namespace MoneyTrack
{
    public partial class ViewController : UIViewController
    {

        char moneySym = '₽';

        SaveLoad SaveLoad = new SaveLoad();

        public ViewController(IntPtr handle) : base(handle)
        {
           // ScrollingButtonsController();
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            OnStartUp();
            MoneyAll.Text = SaveLoad.Load() + moneySym;
        }

        public override void DidReceiveMemoryWarning ()
        {
            
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
            
        }

        partial void Go_TouchUpInside(UIButton sender)
        {

            int action = 1;
            switch(AddSubtract.SelectedSegment)
            {
                case 0: action = -1; break;
                case 1: action = 1; break;
            }
            if (MoneyEntered.Text == "")
            {
                UIAlertView alert = new UIAlertView()
                {
                    Title = "Not valid count",
                };
                alert.AddButton("OK");
                alert.Show();
            }
            else
            {
                float money = float.Parse(MoneyAll.Text.Remove(MoneyAll.Text.Length-1)) + float.Parse(MoneyEntered.Text.Replace(',', '.')) * action;

                PleaseChangeIt();


                if (action > 0)
                {
                    hs1.Text = "+" + MoneyEntered.Text + moneySym;
                    hs1.TextColor = UIColor.Green;
                }
                else
                {
                    hs1.Text = "-" + MoneyEntered.Text + moneySym;
                    hs1.TextColor = UIColor.Red;
                }


                MoneyEntered.Text = "";
                MoneyAll.Text = money.ToString() + moneySym;

                
                NSUserDefaults.StandardUserDefaults.SetString(money.ToString(), "moneyAll");
                NSUserDefaults.StandardUserDefaults.Synchronize();
            }
        }

       

        public void PleaseChangeIt()
        {

            hs4.Text = hs3.Text;
            hs4.TextColor = hs3.TextColor;

            hs3.Text = hs2.Text;
            hs3.TextColor = hs2.TextColor;

            hs2.Text = hs1.Text;
            hs2.TextColor = hs1.TextColor;
        }

        public void OnStartUp()
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            try
            {
                Console.WriteLine("Try is succefull!");
                string[] loaded_data = new string[10];
                XmlDocument doc = new XmlDocument();
                doc.Load(documentsPath + "/profile.xml");
                XmlNode child = doc.SelectSingleNode("head/Settings");
                if (child != null)
                {
                    Console.WriteLine("Child != null");
                    XmlNodeReader nr = new XmlNodeReader(child);
                    int i = 0;
                    int num = 0;
                    while (nr.Read())
                        if (i == 2)
                        {
                            loaded_data[num] = nr.Value;
                            Console.WriteLine("Loaded data: " + loaded_data[num]);
                            num++;
                            i = 0;
                        }
                        else
                        {
                            i++;
                        }
                }


            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
                XmlTextWriter invWriter = new XmlTextWriter(documentsPath + "/profile.xml", System.Text.Encoding.UTF8);
                invWriter.WriteStartDocument();
                invWriter.WriteStartElement("head");
                invWriter.WriteEndElement();
                invWriter.Close();
                XmlDocument document = new XmlDocument();
                document.Load(documentsPath + "/profile.xml");
                XmlNode element = document.CreateElement("Settings");
                document.DocumentElement.AppendChild(element); // указываем родителя
                XmlAttribute attribute = document.CreateAttribute("name"); // создаём атрибут
                attribute.Value = "Atribute"; // устанавливаем значение атрибута

                XmlNode x = document.CreateElement("MoneyAll"); // даём имя
                x.InnerText = "0"; // и значение
                element.AppendChild(x); // и указываем кому принадлежит

                document.Save(documentsPath + "/profile.xml");

                MoneyAll.Text = "0" + moneySym;
                OnStartUp();
            }

        }

     /*   public void Write(string MoneyAll)
        {

            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            XmlTextWriter invWriter = new XmlTextWriter(documentsPath + "/profile.xml", System.Text.Encoding.UTF8);
            invWriter.WriteStartDocument();
            invWriter.WriteStartElement("head");
            invWriter.WriteEndElement();
            invWriter.Close();
            XmlDocument document = new XmlDocument();
            document.Load(documentsPath + "/profile.xml");
            XmlNode element = document.CreateElement("Settings");
            document.DocumentElement.AppendChild(element); // указываем родителя
            XmlAttribute attribute = document.CreateAttribute("name"); // создаём атрибут
            attribute.Value = "Atribute"; // устанавливаем значение атрибута

            XmlNode x = document.CreateElement("MoneyAll"); // даём имя
            x.InnerText = MoneyAll; // и значение
            element.AppendChild(x); // и указываем кому принадлежит

            document.Save(documentsPath + "/profile.xml");

            Console.WriteLine("Succefull write!");
        } */
    }
}